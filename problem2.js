

function problem2(inventory){
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    

    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    const n=inventory.length-1;

    console.log(`Last car is a ${inventory[n].car_make} ${inventory[n].car_model}`)
}

export default problem2;