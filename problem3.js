function problem3(inventory) {
    if (!Array.isArray(inventory)) {
        console.log("Array not found");
        return;
    }

    if (inventory.length === 0) {
        console.log("Data Not Present");
        return;
    }

    const sortedInventory = [...inventory];

    for (let index = 0; index < sortedInventory.length; index++) {
        let currentElement = sortedInventory[index];
        let j_index = index - 1;

        while (j_index >= 0 && sortedInventory[j_index].car_model.toLowerCase() > currentElement.car_model.toLowerCase()) {
            sortedInventory[j_index + 1] = sortedInventory[j_index];
            j_index--;
        }

        sortedInventory[j_index + 1] = currentElement;
    }

    console.log(sortedInventory);
}

export default problem3;
