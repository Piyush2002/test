
function problem1(inventory){
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    
    // console.log(Array.isArray(inventory));

    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    let bool=false;
    for(let index=0;index<inventory.length;index++){

        if(inventory[index].id===33){
            console.log(`Car ${index+1} is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`);
            bool=true;
        }
    }

    if(bool===false) console.log("Car Not Found");
}


export default problem1;