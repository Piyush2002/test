
const newinventory=[];
function problem4(inventory){
    
    if(!(Array.isArray(inventory))){
        return console.log("Array not found");
    }
    
    if(inventory.length == 0){
        return console.log("Data Not Present");
    }

    for(let index=0;index<inventory.length;index++){

        newinventory.push(inventory[index].car_year);

    }

    return newinventory;
}

export default problem4;